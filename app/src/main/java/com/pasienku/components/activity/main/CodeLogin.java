package com.pasienku.components.activity.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pasienku.components.R;
import com.pasienku.components.activity.assets.CodeConfig;
import com.pasienku.components.activity.assets.CodeSharedPreferenceHelper;
import com.pasienku.components.utils.Tools;

import java.util.Hashtable;
import java.util.Map;

public class CodeLogin extends AppCompatActivity {

    CodeSharedPreferenceHelper codeSharedPreferenceHelper, csp;
    Context context;

    EditText editUsernm, editPasswd;

    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        Tools.setSystemBarColor(this, android.R.color.white);
        Tools.setSystemBarLight(this);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Cek data ke server ...");
        pDialog.setCancelable(false);

        editUsernm = (EditText)findViewById(R.id.editTextLoginUsernm);
        editPasswd = (EditText)findViewById(R.id.editTextLoginPasswd);

        cekLogin();
        onClick();
    }

    public void cekLogin(){
        /*SharedPreferences prefs = getSharedPreferences("MY_PREF", MODE_PRIVATE);
        String shared_id_dokter = prefs.getString("id_dokter", "DEFAULT");*/
        if (csp.getIdDokter()!="") {
            startActivity(new Intent(getBaseContext(), CodeHome.class));
            finish();
        }
    }

    public void onClick(){
        Button btnLogin = (Button)findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getBaseContext(), CodeHome.class));
                //Toast.makeText(getBaseContext(), "Login", Toast.LENGTH_SHORT).show();
                if (editUsernm.getText().toString().equals("") || editPasswd.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Mohon diisi semua", Toast.LENGTH_SHORT).show();
                } else {
                    prosesCekAkun();
                    //Toast.makeText(getApplicationContext(), "Terisi", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //menampilkan progress dialog
    void showProgressDialog(){
        if(!pDialog.isShowing())pDialog.show();
    }

    //menutup progress dialog
    void dismissProgressDialog(){
        if(pDialog.isShowing())pDialog.dismiss();
    }

    public void prosesCekAkun(){
        showProgressDialog();
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        StringRequest requestUpload = new StringRequest(Request.Method.POST, CodeConfig.URL_CEK_AKUN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissProgressDialog();
                        //Toast.makeText(getBaseContext(), response,Toast.LENGTH_SHORT).show();
                        setIdDokter(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Tidak ada koneksi",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<>();
                params.put("usernm",editUsernm.getText().toString());
                params.put("passwd",editPasswd.getText().toString());
                return params;
            }
        };
        requestUpload.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestUpload);
    }

    public void setIdDokter(String id_dokter){
        if (id_dokter.equals("0")) {
            Toast.makeText(getBaseContext(), "Username atau password tidak cocok", Toast.LENGTH_SHORT).show();
        } else {
            /*Intent i = new Intent(getBaseContext(), CodeHome.class);
            i.putExtra("id_dokter", id_dokter);
            startActivity(i);*/

            /*SharedPreferences.Editor editor = getSharedPreferences("MY_PREF", MODE_PRIVATE).edit();
            editor.putString("id_dokter", id_dokter);
            editor.apply();*/

            codeSharedPreferenceHelper = new CodeSharedPreferenceHelper(
                    context, CodeLogin.this,
                    id_dokter
            );

            startActivity(new Intent(getBaseContext(), CodeHome.class));
            finish();
        }
    }
}
