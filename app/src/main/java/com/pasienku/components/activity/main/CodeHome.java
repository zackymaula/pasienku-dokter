package com.pasienku.components.activity.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pasienku.components.R;
import com.pasienku.components.activity.assets.CodeConfig;
import com.pasienku.components.activity.assets.CodeSharedPreferenceHelper;
import com.pasienku.components.adapter.AdapterListBasic;
import com.pasienku.components.adapter.AdapterListPasien;
import com.pasienku.components.data.DataGenerator;
import com.pasienku.components.model.Pasien;
import com.pasienku.components.model.People;
import com.pasienku.components.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CodeHome extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActionBar actionBar;
    private Toolbar toolbar;

    private RecyclerView recyclerView;
    private AdapterListPasien mAdapter;

    String id_dokter, dokter_nama, dokter_alamat, dokter_spesialis, dokter_tanggal, dokter_antrian;

    //TextView txtNamaDokter, txtAlamatDokter, txtSpesialisDokter, txtTanggalSkrg, txtAntrianDokter;

    public static List<Pasien> items =  new ArrayList<>();

    CodeSharedPreferenceHelper codeSharedPreferenceHelper, csp;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);

        items.clear();
        getDataShared();
        getDokter();
        initToolbar();
    }

    public void getDataShared(){
        /*Intent intent = getIntent();
        id_dokter = intent.getStringExtra("id_dokter");*/
        /*SharedPreferences prefs = getSharedPreferences("MY_PREF", MODE_PRIVATE);
        id_dokter = prefs.getString("id_dokter", null);*/
        //Toast.makeText(getBaseContext(), id_dokter, Toast.LENGTH_SHORT).show();

        id_dokter = csp.getIdDokter();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Pasienku");
    }

    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        nav_view.setNavigationItemSelectedListener(this);
        View header = nav_view.getHeaderView(0);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                //Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
                //actionBar.setTitle(item.getTitle());
                //drawer.closeDrawers();
                setLogout();
                return true;
            }
        });

        // open drawer at start
        drawer.openDrawer(GravityCompat.START);
        TextView txtNamaDokter = (TextView)header.findViewById(R.id.textViewDrawerNama);
        TextView txtAlamatDokter = (TextView)header.findViewById(R.id.textViewDrawerAlamat);
        TextView txtSpesialisDokter = (TextView)header.findViewById(R.id.textViewDrawerSpesialis);
        TextView txtTanggalSkrg = (TextView)header.findViewById(R.id.textViewDrawerTanggal);
        TextView txtAntrianDokter = (TextView)header.findViewById(R.id.textViewDrawerJumlahAntrian);
        txtNamaDokter.setText(dokter_nama);
        txtAlamatDokter.setText(dokter_alamat);
        txtSpesialisDokter.setText(dokter_spesialis);
        txtTanggalSkrg.setText(dokter_tanggal);
        txtAntrianDokter.setText(dokter_antrian);
    }

    private void setListPasien() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewPasien);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        //List<Pasien> items = getPasienData(this);

        //set data and list adapter
        mAdapter = new AdapterListPasien(this, items);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterListPasien.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Pasien obj, int position) {
                Toast.makeText(getBaseContext(), obj.nama, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setLogout(){
        /*SharedPreferences.Editor editor = getSharedPreferences("MY_PREF", MODE_PRIVATE).edit();
        editor.putString("id_dokter", "");
        editor.apply();*/

        codeSharedPreferenceHelper = new CodeSharedPreferenceHelper(
                context, CodeHome.this,
                ""
        );

        startActivity(new Intent(getBaseContext(), CodeLogin.class));
        finish();
    }

    public void getDokter(){
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                CodeConfig.URL_GET_DOKTER + id_dokter,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray item = response.getJSONArray("result_dokter");
                            JSONObject person = item.getJSONObject(0);

                            dokter_nama = person.getString("nama");
                            dokter_spesialis = person.getString("spesialis");
                            dokter_alamat = person.getString("alamat");

                            JSONArray item2 = response.getJSONArray("result_jumlah_antrian");
                            JSONObject person2 = item2.getJSONObject(0);

                            dokter_antrian = person2.getString("jumlah_antrian");

                            JSONArray item3 = response.getJSONArray("result_tanggal");
                            JSONObject person3 = item3.getJSONObject(0);

                            dokter_tanggal = person3.getString("tanggal");

                            JSONArray item4 = response.getJSONArray("result_list_antrian");
                            for (int i = 0; i < item4.length(); i++) {
                                JSONObject person4 = item4.getJSONObject(i);

                                Pasien obj = new Pasien();
                                obj.id_antrian = person4.getString("id_antrian");
                                obj.nama = person4.getString("nama");
                                obj.alamat = person4.getString("alamat");
                                items.add(obj);
                            }
                            setListPasien();
                            initNavigationMenu();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(),"Pasien kosong",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(),"Tidak ada koneksi internet",Toast.LENGTH_SHORT).show();
                    }
                }
        );
        jsonObjectRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(CodeHome.this).add(jsonObjectRequest);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != android.R.id.home) {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }*/

}
