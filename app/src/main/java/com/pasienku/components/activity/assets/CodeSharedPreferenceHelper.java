package com.pasienku.components.activity.assets;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class CodeSharedPreferenceHelper {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public CodeSharedPreferenceHelper(Context context, Activity activity,
                                      String id_dokter) {
        preferences = context.getSharedPreferences("MY_PREF", Context.MODE_PRIVATE);
        editor = this.preferences.edit();
        editor.putString("id_dokter", id_dokter);
        editor.commit();
    }

    public CodeSharedPreferenceHelper(Context context){
        this.preferences = context.getSharedPreferences("MY_PREF",Context.MODE_PRIVATE);
    }

    public String getIdDokter() {
        return preferences.getString("id_dokter","");
    }
}
